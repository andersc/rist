![librist logo](librist_logo.png)

# rist

Command line applications that use the librist library.

They can be used to integrate the rist protocol into your workflow without having to write any code.

The canonical repository URL for this repo is https://code.videolan.org/rist/rist

This project is partially funded by the SipRadius LLC.

## Goal and Features

The goal of this project is to provide a command line application for windows/linux/mac.

## License

**mpeg2rist and rist2mpeg** are released under a very liberal license, a contrario from the other VideoLAN projects, so that it can be embedded anywhere, including non-open-source software.

The reasoning behind this decision is the same as for libvorbis, see [RMS on vorbis](https://lwn.net/2001/0301/a/rms-ov-license.php3).

# Roadmap

The plan is the following:

### Reached
1. Complete C implementation of the application,
2. Compiled and tested under on linux/osx (x86),

### On-going
2. Port/test on more platforms,
3. Provide sample applications for other languages,
4. Test compilation and runtime under window,

# Contribute
Currently, we are looking for help from:
- Someone to provide librist usage examples for python, php and any other scripting language
- Developers (on any language or framework) willing to port these applications from C to those languages and/or platforms
- testers

Our contributions guidelines are quite strict. We want to build a coherent codebase to simplify maintenance and achieve the highest possible speed.

Notably, the codebase is in pure C and asm.

We are on Telegram, on the rist_users and librist_developers channels.

See the [contributions document](CONTRIBUTING.md).

## CLA

There is no CLA.

VideoLAN will only have the collective work rights.

## CoC

The [VideoLAN Code of Conduct](https://wiki.videolan.org/CoC) applies to this project.

# Compile using meson/nija (preferred method)

1. Install [Meson](https://mesonbuild.com/) (0.47 or higher), [Ninja](https://ninja-build.org/), and, for x86\* targets, [nasm](https://nasm.us/) (2.14 or higher)
2. Compile librist (follow the instructions on that README)
3. Run `sudo ninja install` on the librist build folder
4. Clone the rist project: `git clone https://code.videolan.org/rist/rist.git`
5. Run `mkdir build && cd build` to create a build directory and enter it
6. Run `meson ..` to configure meson, add `--default-library=static` if static linking is desired
7. Run `ninja` to compile

# Compile using cmake/make (alternative method)

1. Clone the rist project: `git clone https://code.videolan.org/rist/rist.git`
2. Run `(cd rist && cmake . && make)` to compile (it will fetch and compile librist automatically in this step as well)
3. For step 2, you could use the following instead of `cmake .` to pick the build type: `cmake -DCMAKE_BUILD_TYPE=Debug .` or `cmake -DCMAKE_BUILD_TYPE=Release .`

# Support

This project is partially funded by the SipRadius LLC.

This company can provide support and integration help, should you need it.

# FAQ

## Is rist a recursive acronym?

- Yes, RIST stands for Reliable Internet Stream Transport

## Can I help?

- Yes. See the [contributions document](CONTRIBUTING.md).

## I am not a developer. Can I help?

- Yes. We need testers, bug reporters, and documentation writers.

## What about the packet recovery patents?

- These are just simple command line applications that use the librist library. It is all trivial un-patenteable code.

## Will you care about <my_arch>? <my_os>?

- We do, but we don't have either the time or the knowledge. Therefore, patches and contributions welcome.

## How do I compile it on ubuntu 18.04?

1. Install dependencies: `sudo apt install meson autoconf automake build-essential cmake`
2. Go to your home folder: `cd $HOME`
3. Compile and install nasm: `tar xjvf nasm-2.14.02.tar.bz2 && cd nasm-2.14.02 && ./autogen.sh && ./configure && make && make install`
4. Clone librist repo: `git clone https://code.videolan.org/rist/librist.git`
5. Clone rist repo: `git clone https://code.videolan.org/rist/rist.git`
6. Go to librist folder: `cd librist`
7. Create build dir: `mkdir build && cd build`
8. Configure: `meson ..`
9. Compile and install: `sudo ninja install`
10. Go to rist folder: `cd $HOME && cd rist`
11. create build folder: `mkdir build && cd build`
12. Configure: `meson ..`
13. Compile: `ninja`

## How do I compile it on OSX?

1. Download and install cmake from: `https://github.com/Kitware/CMake/releases/download/v3.17.0-rc3/cmake-3.17.0-rc3-Darwin-x86_64.dmg`
2. Open a terminal window and clone the rist repo: `git clone https://code.videolan.org/rist/rist.git`
3. Enter the newly created rist folder and run "./osx_compile.sh"
